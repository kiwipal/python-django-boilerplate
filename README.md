# Dockerizing Django with Postgres, Gunicorn, and Nginx

This is a test environment, inluding a prod version to be able to test using Nginx + Unicorn.
To deploy on prod, you should consider removing Postgres from the docker compose and use an external posgres database instead.

## Developement

Dev environment runs with the django debug setting set to `true`.
Therefore, static images are served by django and there is no need to collect static assets.

### Important

Prod env uses Flake8 as a linter and the container build will stop if an error is found (reported in the console). 
Look at the container's log to check if the database started properly. 
Django will wait the Postgres server to be ready, don't be impatient... 

### Docker Commands

Run container:

    $ docker-compose up -d --build

Stop container: 

    $ docker-compose down

Test page on http://localhost:8001 

## Production

Prod env uses gunicorn + nginx instead of the django built-in server.
No mounted folders. To apply changes, the image must be re-built each time.

### IMPORTANT:

Static assets are collected when the image is built and stored in the `/assets/` folder. 
The database will be initialized empty and must be populated. 
For persistent datas, it would be better to remove Postgres from the docker-compose and uses an external Postgres database instead. 

### Docker Commands

Run container:
 
    $ docker-compose -f docker-compose.prod.yml up -d --build
 
Stop container
 
    $ docker-compose -f docker-compose.prod.yml down -v
 
Test page : http://localhost:1337

## Coverage

    $ coverage run --source='.' manage.py test myapp 

    $ coverage html

## Unit test (if not using coverage) 

    $ python manage.py test
