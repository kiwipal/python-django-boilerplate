# SSL Certificate

When building images, the docker script (entrypoint.sh) will create an autosigned certificate if no certificates exists in the folder:

- certificate.crt
- certificate.key

> The autosigned certificate will issue a warning on local environment and are only useful for dev env.



## Production mode

When creating the image for prod, the certificate will be used, so to use a real one (a let's encrypt certificate for example). Just replace the files in the `/app/certificate` folder before builfing the image.

The certificate is declared in the NGINX conf file : `/nginx/nginx.conf`
