#!/bin/sh

# No SSL Certificate auto generated for production: be sure to add them in /app/certificates

# Waiting for Postgre database to bu up
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# Run webpack
npm run build && python manage.py collectstatic --clear --no-input

# Collect Static
#python manage.py collectstatic --no-input

# Prepare and make migration for Django database model
python manage.py makemigrations --no-input
python manage.py migrate --no-input

# Run coverage and create a report
coverage run --source='.' manage.py test myapp
coverage report
coverage html
echo "Coverage Report HTML stored in: `/coverage_html_report`"

exec "$@"
