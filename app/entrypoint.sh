#!/bin/sh

# Create auto-signed SSL certificate if missing
if [ ! -f /usr/src/app/certificate/certificate.crt ]; then
    openssl req -x509 -nodes -days 365 -subj  "/C=CA/ST=QC/O=Kiwipal/CN=myapp.com" -newkey rsa:2048 -keyout /usr/src/app/certificate/certificate.key -out /usr/src/app/certificate/certificate.crt
fi

# Waiting for Postgre database to bu up
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL Service Started!"
fi

# Uncomment this line to flush database on start
# python manage.py flush --no-input

# Run webpack (`dev` is defined in package.json and use a nodemon to auto reload changes on the `webpack.config.js` file)
# Changes on any files in the ./src folder will reload the assets (scss, js...)
npm run dev &

# Collect static elements
python manage.py collectstatic --no-input

# Prepare and make migration for Django database model
python manage.py makemigrations --no-input
python manage.py migrate --no-input

# Run coverage and create a report
coverage run --source='.' manage.py test myapp
coverage report
coverage html
echo "Coverage Report HTML stored in: `/coverage_html_report`"

exec "$@"
