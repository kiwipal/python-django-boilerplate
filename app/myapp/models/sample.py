from django.db import models
from django.utils import timezone
from myapp.modules.log import error_log

# Debug Model Class extend Model
class Sample(models.Model):
    
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=25)
    is_great = models.CharField(max_length=1)
    date_creation = models.DateTimeField()
        
    def getSamples(self):
        return self.objects.all().order_by("-date_creation")

    def add(self, name, is_great=1, date_creation=timezone.now()):
        self.objects.create(name = name, date_creation=date_creation, is_great=is_great)
        return True
