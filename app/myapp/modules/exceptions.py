from django.http import HttpResponseRedirect
from django.http import Http404
from django.shortcuts import redirect

# Extend the Exception Class
class CustomError(Exception):
    pass

# Custom Missing var exception
class MissingVar(CustomError):
    pass