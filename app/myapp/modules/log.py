
import logging
import logging.config
from django.conf import settings

# Silent asyncio to prevenbt a default message to appear in log
logging.getLogger('asyncio').setLevel(logging.WARNING)

# Logger config
logger = logging.getLogger(__name__)
handler = logging.FileHandler(settings.BASE_DIR + '/myapp/logs/custom.log')
formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-s %(message)s', '%Y-%m-%d %H:%M:%S')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

# Error log method shortcut
# @param string m
def error_log(m):
    logger.debug(m)