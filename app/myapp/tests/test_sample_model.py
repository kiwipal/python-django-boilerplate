from django.test import TestCase
from django.utils import timezone
from myapp.models import Sample

class TestSampleModel(TestCase):
    
    def test_sample_add(self):

        # Create a post
        isSampleCreated = Sample.add(Sample, name="Test Location", is_great=0, date_creation=timezone.now())

        # Is Location created
        self.assertEqual(isSampleCreated, True)