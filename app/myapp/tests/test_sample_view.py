from django.test import TestCase
from django.utils import timezone
from myapp.models import Sample

class TestSampleView(TestCase):
    
    def test_sample_page_loads_properly(self):

        response = self.client.get('https://localhost:8001/sample/')
        self.assertEqual(response.status_code, 200)

    def test_sample_page_with_params_loads_properly(self):

        response = self.client.get('https://localhost:8001/sample/?extra=1')
        self.assertEqual(response.status_code, 200)