from django.conf.urls import url
from django.urls import path
from myapp.views import *

urlpatterns = [
    path('', home.HomeView.as_view(), name='home'),
    path('sample/', sample.SampleView.as_view(), name='sample'),
]