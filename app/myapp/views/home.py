from django.shortcuts import render
from django.views.generic import TemplateView # Import TemplateView
from django.http import HttpResponseRedirect

class HomeView(TemplateView):

    template_name = "pages/index.html",
    context = {}

    # Init
    def __init__(self):
        self.__setContext()

    # Set Context
    def __setContext(self):

        self.context.update({
            "app_name": "My App"
        })

    # Get method
    def get(self, request):

        return render(request, self.template_name, self.context)



