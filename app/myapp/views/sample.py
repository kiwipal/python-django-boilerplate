from django.shortcuts import render
from django.views.generic import TemplateView # Import TemplateView
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
from django.conf import settings # import the settings file
from django import forms
from django.urls import reverse
from myapp.models import Sample
from myapp.modules.log import error_log
from myapp.modules.exceptions import *
from django.http import Http404
from django.shortcuts import redirect
import mistune # markdown module
import os

@method_decorator(csrf_protect, name="post")
class SampleView(TemplateView):

    template_name = "pages/sample.html"
    context = {}

    # Init
    def __init__(self):

        self.__setContext()

    # Set Context
    def __setContext(self):

        #raise MissingVar("Exemple d'exception perso !")
        #Http404("No MyModel matches the given query.")

        self.context.update({
            "app_name": settings.APP["name"],
            "collect_static_dir": os.path.join(settings.BASE_DIR, "collect/static"),
            "base_dir": settings.BASE_DIR,
            "path": reverse('sample'),
            "author": {
                "first_name": "guillaume",
                "last_name": "le nistour"
            },
            "markdown": mistune.html("## Example de Markdown en HTML")
        })

    # Get method
    def get(self, request):

        # Example of try catch
        try:
            if request.GET["extra"]:
                self.context.update({
                    "extra": request.GET["extra"]
                })
        except:
            pass
        
        # If redirect home
        if 'redirect_home' in request.GET:
            return redirect('home', permanent=False)

        if 'redirect_404' in request.GET:
            raise Http404(request, "Custom message !")

        # Get Samples form model
        self.context.update({
            "samples": Sample.getSamples(Sample)
        })
        
        # Log
        error_log("Render View !")

        return render(request, self.template_name, self.context)

    # Post method
    def post(self, request):
        Sample.add(Sample, request.POST["new_sample"])

        return HttpResponseRedirect("/sample")

class SimpleForm(forms.Form):
    name = forms.CharField(max_length=25)
    