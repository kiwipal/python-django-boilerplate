from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

# Use admin url and include myapp url
urlpatterns = [
    path("admin/", admin.site.urls),
    url(r'^', include('myapp.urls'))
]

# For dev env, use direct media path
if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
