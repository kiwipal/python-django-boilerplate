
import React from 'react';
import ReactDOM from 'react-dom';
import { sample } from './sample';
import { sample2 } from './sample2';
import styles from './app.module'
import './global'

console.log(sample("Hello World Man 3!"));
console.log(sample2(2,8));

const App = () => {
    return <h1>My React App</h1>;
  };
  
ReactDOM.render(<App />, document.querySelector("#root"));