

Composition Vs Inheritance


- Modifier Gitlab CI/DC pour faire le build et faire tourner les tests unitaire
- Installer les babel and co
- Installer un système de test unitaire en JS
- Intaller un système de test unitaire end to end
- Installer un material kit
- Préparer un fichier de conf kubernetes
- Créer un compte amazon
- Mettre en ligne chez Amazon


J'ai lu que pour empêcher les devs de toucher à la conf de CI/CD, on peut la stocker sur un autre repo vérouillé.

Il faudra trouver comment utiliser les secrets dans Gitlab

Il faudra voir l'intégration avec Asana

On peut tester le scénario avec des messages de console
Puis avec des messages slack
Puis avec les tests
Puis avec les merge
Puis avec les push vers serveur

- Pusher sur une branche autre que stage ou master ne trigger pas le CI/CD
    C'est simplement du stockage de code
- Pusher sur la stage
    Eviter que ce soit possible si un test est en cours ?
        Idéalement, chainner
    Trigger CI/CD
    En cas d'absence d'erreurs
        Crée un nouveau serveur à la volée chez Amazon
        Déploie le site sur le nouveau serveur de test
        Envoi un Slack au dev avec l'url ou tester
- Créer un Pull Request
    S'il est accepté
        Supression du serveur de test
        merge du code code stage > master
        Pas besoin de faire tourner les tests, ils ont déjà été passés ?
        Attribution d'un tag
        Déploiement d'une image versionnée et d'une latest sur docker hub de gitlab
        Déploiement sur le serveur de prod
    Message Slack



Installer Kompose
$ curl -L https://github.com/kubernetes/kompose/releases/download/v1.21.0/kompose-darwin-amd64 -o kompose
$ chmod +x kompose
$ sudo mv ./kompose /usr/local/bin/kompose